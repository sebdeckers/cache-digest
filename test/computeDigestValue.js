import test from 'ava'
import computeDigestValue from '../src/computeDigestValue'

const fixtures = [
  {
    given: [
      false, // validators
      ['https://example.com/style.css', 'https://example.com/jquery.js']
        .map((url) => [url, null]), // tuples [url, etag|null]
      2 ** 7 // probability (1/P)
    ],
    expected: new Uint8Array([0x09, 0xd6, 0x50, 0xe0])
  }
]

for (const {given, expected} of fixtures) {
  test('computeDigestValue', (t) => {
    const actual = computeDigestValue(...given)
    t.deepEqual(actual, expected)
  })
}
