import test from 'ava'
import queryDigestValue from '../src/queryDigestValue'

const fixtures = [
  {
    digestValue: [0x01, 0xf7, 0x40],
    tuples: [
      {
        url: 'https://example.com/style.css',
        etag: '',
        expected: true
      },
      {
        url: 'https://example.com/not_in_the_digest',
        etag: '',
        expected: false
      }
    ],
    validators: false
  },
  {
    digestValue: [0x09, 0xd6, 0x50, 0xe0],
    tuples: [
      {
        url: 'https://example.com/style.css',
        etag: '',
        expected: true
      },
      {
        url: 'https://example.com/jquery.js',
        etag: '',
        expected: true
      }
    ],
    validators: false
  },
  {
    digestValue: [0x09, 0x16, 0x80],
    tuples: [
      {
        url: 'https://example.com/style.css',
        etag: '',
        expected: true
      },
      {
        url: 'https://example.com/jquery.js',
        etag: '',
        expected: true
      }
    ],
    validators: false
  }
]

test('queryDigestValue', (t) => {
  for (const {digestValue, tuples, validators} of fixtures) {
    for (const {url, etag, expected} of tuples) {
      const calculate = () => queryDigestValue(
        Uint8Array.from(digestValue),
        url,
        etag,
        validators
      )
      if (expected === true) {
        const actual = calculate()
        t.is(actual, expected)
      } else if (expected === false) {
        t.throws(calculate)
      }
    }
  }
})
