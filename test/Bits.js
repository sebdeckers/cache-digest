import test from 'ava'
import Bits from '../src/Bits'

test('Bits takes bytes', (t) => {
  const bytes = new Uint8Array([111, 11, 1])
  t.notThrows(() => new Bits(bytes))
})

test('Bits takes buffers', (t) => {
  const buffer = new Buffer([111, 11, 1])
  t.notThrows(() => new Bits(buffer))
})

test('Bits does not take arrays', (t) => {
  const array = [111, 11, 1]
  t.throws(() => new Bits(array), 'Argument must be an Uint8Array')
})

test('Bits does not take strings', (t) => {
  const string = '111111'
  t.throws(() => new Bits(string), 'Argument must be an Uint8Array')
})

test('read 1 to 31 bits', (t) => {
  for (let howMany = 1; howMany <= 31; howMany += 1) {
    const bits = new Bits(new Uint8Array([
      0b11111111,
      0b11111111,
      0b11111111,
      0b11111111
    ]))
    const actual = bits.read(howMany)
    const expected = (2 ** howMany) - 1
    t.is(actual, expected)
  }
})

test('read consecutively', (t) => {
  const bits = new Bits(new Uint8Array([
    0b10001100,
    0b11101111,
    0b01111111,
    0b01010101
  ]))
  t.is(bits.read(3), 0b100)
  t.is(bits.read(5), 0b01100)
  t.is(bits.read(4), 0b1110)
  t.is(bits.read(4), 0b1111)
  t.is(bits.read(1), 0b0)
  t.is(bits.read(15), 0b111111101010101)
})

test('read 0 bits', (t) => {
  const bits = new Bits(new Buffer([]))
  t.throws(() => bits.read(0), 'Length must be greater than 0')
})

test('read more than 31 bits', (t) => {
  const bits = new Bits(new Buffer([]))
  t.throws(() => bits.read(32), 'Length must not be greater than 31')
})

test('read more bits than are available', (t) => {
  const bits = new Bits(new Buffer([0b11111111]))
  t.throws(() => bits.read(9), 'Out of bounds')
})

test('count consecutive bits', (t) => {
  const bits = new Bits(new Buffer([
    0b10011100,
    0b00111110
  ]))
  t.is(bits.count(1), 1)
  t.is(bits.count(0), 2)
  t.is(bits.count(1), 3)
  t.is(bits.count(0), 4)
  t.is(bits.count(1), 5)
})

test('count only bits', (t) => {
  const bits = new Bits(new Buffer([]))
  t.throws(() => bits.count(2), 'Bit must be 0 or 1')
})

test('skip jumps ahead', (t) => {
  const bits = new Bits(new Buffer([
    0x00000000,
    0x00000001
  ]))
  bits.read(10)
  bits.skip(5)
  t.is(bits.read(1), 0b1)
})

test('skip only integer lengths', (t) => {
  const bits = new Bits(new Buffer([]))
  t.throws(() => bits.skip('💩'), 'Length must be an integer')
})

test('skip never goes backwards', (t) => {
  const bits = new Bits(new Buffer([]))
  t.notThrows(() => bits.skip(0))
  t.throws(() => bits.skip(-1), 'Length must be positive')
})

test('skip but never too far', (t) => {
  const bits = new Bits(new Buffer([
    0x00000000,
    0x00000000
  ]))
  t.notThrows(() => bits.skip(16))
  t.throws(() => bits.skip(1), 'Out of bounds')
})

test('isExhausted is correct from start to finish', (t) => {
  const bits = new Bits(new Buffer([
    0x00000000,
    0x00000000
  ]))
  for (let i = 1; i <= 16; i += 1) {
    t.false(bits.isExhausted())
    bits.skip(1)
  }
  t.true(bits.isExhausted())
})

test('isExhausted is fine with empty buffers', (t) => {
  const bits = new Bits(new Buffer([]))
  t.true(bits.isExhausted())
})
