import test from 'ava'
import cacheDigest from '..'

test('interface', (t) => {
  t.is(typeof cacheDigest.computeDigestValue, 'function')
  t.is(typeof cacheDigest.computeHashValue, 'function')
  t.is(typeof cacheDigest.queryDigestValue, 'function')
})
