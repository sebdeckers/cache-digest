import test from 'ava'
import computeHashValue from '../src/computeHashValue'

const fixtures = [
  {
    given: [
      ['https://example.com/style.css'],
      false,
      2 ** 5,
      2 ** 7
    ],
    expected: 'baf'
  },
  {
    given: [
      [''], false, 1, 2 ** 8
    ],
    expected: 'e3'
  },
  {
    given: [
      [''], false, 1, 2 ** 5
    ],
    expected: '1c'
  },
  {
    given: [
      ['hello world'], false, 1, 2 ** 11
    ],
    expected: '60e'
  },
  {
    given: [
      ['💩', ''], false, 8, 2 ** 28
    ],
    expected: '3bb91942'
  }
]

for (const {given, expected} of fixtures) {
  test('computeHashValue', (t) => {
    const actual = computeHashValue(...given)
    t.is(actual.toString(16), expected)
  })
}
