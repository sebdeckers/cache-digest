'use strict'

const createHash = require('crypto').createHash

// https://tools.ietf.org/html/draft-kazuho-h2-cache-digest-01#section-2.1.2
// 2.1.2.  Computing a Hash Value

// Given:
module.exports = function computeHashValue (
  // "URL", an array of characters
  // "ETag", an array of characters
  tuple,

  // "validators", a boolean
  validators,

  // "N", an integer
  n,

  // "P", an integer
  p
) {
  let url = tuple[0]
  let etag = tuple[1]

  if (url === undefined) url = ''
  if (etag === undefined) etag = ''
  if (validators === undefined) validators = false
  if (n === undefined) n = 1
  if (p === undefined) p = 1

  // "hash-value" can be computed using the following algorithm:
  let hashValue

  // Let "key" be "URL" converted to an ASCII string by percent-
  // encoding as appropriate [RFC3986].
  const key = appropriatelyPercentEncode(url)

  // If "validators" is true and "ETag" is not null:
  if (validators === true && etag !== null) {
    // Append "ETag" to "key" as an ASCII string, including both the
    // "weak" indicator (if present) and double quotes, as per
    // [RFC7232] Section 2.3.
    throw Error('Etags are not supported in this implementation') // TODO: Not sure how this works. Examples?
  }

  // Let "hash-value" be the SHA-256 message digest [RFC6234] of
  // "key", expressed as an integer.
  hashValue = new DataView(
    createHash('sha256').update(key).digest().buffer
  ).getUint32(0) // TODO: Spec allows up to 62 bits (n=2**31, p=2**31)

  // Truncate "hash-value" to log2( "N" * "P" ) bits.
  const truncate = Math.log2(n * p)
  if (truncate > 31) throw Error('This implementation only supports up to 31 bit hash values')
  hashValue = (hashValue >> (32 - truncate)) & ((1 << truncate) - 1)

  return hashValue
}

// https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent
// "To be more stringent in adhering to RFC 3986 (which
// reserves !, ', (, ), and *), even though these
// characters have no formalized URI delimiting uses,
// the following can be safely used:"
function appropriatelyPercentEncode (url) {
  const encoded = encodeURI(url)
    // Fix any missing characters as per RFC 3986.
    .replace(/[!'()*]/g, function (character) {
      return '%' + character.charCodeAt(0).toString(16)
    })
  return encoded
}
