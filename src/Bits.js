'use strict'

module.exports = class Bits {
  constructor (bytes) {
    if (!(bytes instanceof Uint8Array)) throw new Error('Argument must be an Uint8Array')

    this._bytes = bytes
    this._length = this._bytes.length * 8
    this._cursor = 0
  }

  read (length) {
    if (isNaN(length)) throw new Error('Length must be an integer')
    if (length < 1) throw new Error('Length must be greater than 0')
    if (length > 31) throw new Error('Length must not be greater than 31')
    if (this._cursor + length > this._length) throw new Error('Out of bounds')

    let result = 0
    for (
      const end = this._cursor + length;
      this._cursor < end;
      this._cursor += 1
    ) {
      const byteIndex = Math.floor(this._cursor / 8)
      const byte = this._bytes[byteIndex]
      const bitIndex = this._cursor % 8
      const mask = 0b10000000 >> bitIndex
      const masked = byte & mask
      const bit = masked >> (8 - bitIndex - 1)
      result |= bit << (end - this._cursor - 1)
    }
    return result
  }

  count (bit) {
    if (bit !== 0 && bit !== 1) throw new Error('Bit must be 0 or 1')
    let counter = 0
    while (this.read(1) === bit) {
      counter += 1
    }
    this._cursor -= 1
    return counter
  }

  skip (length) {
    if (isNaN(length)) throw new Error('Length must be an integer')
    if (length < 0) throw new Error('Length must be positive')
    if (this._cursor + length > this._length) throw new Error('Out of bounds')
    this._cursor += length
  }

  isExhausted () {
    return this._cursor >= this._length
  }
}
