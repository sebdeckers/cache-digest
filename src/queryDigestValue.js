'use strict'

const Bits = require('./Bits')
const computeHashValue = require('./computeHashValue')

// https://tools.ietf.org/html/draft-kazuho-h2-cache-digest-01#section-2.2.1

// 2.2.1.  Querying the Digest for a Value
module.exports = function queryDigestValue (
  //  Given:

  //  o  "digest-value", an array of bits
  digestValue,

  //  o  "URL", an array of characters
  url,

  //  o  "ETag", an array of characters
  etag,

  //  o  "validators", a boolean
  validators
) {
  if (digestValue === undefined) digestValue = new Uint8Array()
  if (url === undefined) url = ''
  if (etag === undefined) etag = ''
  if (validators === undefined) validators = false

  //  we can determine whether there is a match in the digest using the
  //  following algorithm:

  //  1.   Read the first 5 bits of "digest-value" as an integer; let "N"
  //       be two raised to the power of that value.
  digestValue = new Bits(digestValue)
  const n = Math.pow(2, digestValue.read(5))

  //  2.   Read the next 5 bits of "digest-value" as an integer; let "P" be
  //       two raised to the power of that value.
  const p = Math.pow(2, digestValue.read(5))

  //  3.   Let "hash-value" be the result of computing a hash value
  //       (Section 2.1.2).
  const hashValue = computeHashValue([url, etag], validators, n, p)

  //  4.   Let "C" be -1.
  let c = -1

  do {
    // 5.   Read '0' bits from "digest-value" until a '1' bit is found; let
    //       "Q" bit the number of '0' bits.  Discard the '1'.
    const q = digestValue.count(0)
    digestValue.skip(1)

    // 6.   Read log2("P") bits from "digest-value" after the '1' as an
    //       integer; let "R" be its value.
    const r = digestValue.read(Math.log2(p))

    // 7.   Let "D" be "Q" * "P" + "R".
    const d = q * p + r

    // 8.   Increment "C" by "D" + 1.
    c += d + 1

    // 9.   If "C" is equal to "hash-value", return 'true'.
    if (c === hashValue) return true

    // 10.  Otherwise, return to step 5 and continue processing; if no match
    //       is found before "digest-value" is exhausted, return 'false'.
  } while (!digestValue.isExhausted())

  throw new Error(false)
}
