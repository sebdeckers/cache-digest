# cache-digest

Nearly literal implementation of the algorithms described in the Cache Digest proposed specification.

## API

### `computeDigestValue(validators, urls, p)`

Specification:
<https://tools.ietf.org/html/draft-kazuho-h2-cache-digest-01#section-2.1.1>

- `validators` is a boolean. Defaults to `false`. Any truthy value is currently unsupported. See below.

- `urls` is an array of `[url, etag]` tuples, where `url` and `etag` are strings. Etags are not currently supported. See below.

- `p` is an integer. Represents the probability of collisions.

### `computeHashValue([url, etag], validators, n, p)`

Specification:
<https://tools.ietf.org/html/draft-kazuho-h2-cache-digest-01#section-2.1.2>

Returns the hash as an integer. May throw errors on invalid or unsupported input.

- `url` is a string representing the URL. Defaults to empty string. Example: `https://example.com/foo/bar.js`

- `etag` is a string. Defaults to empty string. Currently unsupported. See below.

- `validators` is a boolean. Defaults to `false`. Any truthy value is currently unsupported. See below.

- `n` is an integer. Defaults to `1`. Represents the number of elements in the digest. Affects the size of the hash.

- `p` is an integer. Defaults to `1`. Represents the probability of collisions. Affects the size of the hash.

### `queryDigestValue(digestValue, url, etag, validators)`

Specification:
<https://tools.ietf.org/html/draft-kazuho-h2-cache-digest-01#section-2.2.1>

Returns `true` if the URL is present in the cache digest. Throws an error otherwise.

- `digestValue` is a `Uint8Array` containing the digest.

- `url` is a string representing the URL. Defaults to empty string. Example: `https://example.com/foo/bar.js`

- `etag` is a string. Defaults to empty string. Currently unsupported. See below.

- `validators` is a boolean. Defaults to `false`. Any truthy value is currently unsupported. See below.

## Known Limitations

### 31 bit maximum hash size

Though the specification allows up to 62 bits, this implementation is limitd to 31 bits.

Due to JavaScript's boolean logic operators returning 32-bit signed integers, where one bit is lost to the sign, the maximum size of a hash value is 31 bits.

Keep an eye on the `N` (number of items in the cache digest) and `P` (probability of collisions) values. If their sum exceeds 31, an error will be throw.

Note: This is only applicable to the hash size. Any size of digest value can be generated and queried.

### No Etags support

This may be implemented someday. I am looking for clarification from the specification authors, ideally including test cases (given/expected) to validate the implementation.

## See Also

- [http2server](https://www.npmjs.com/package/http2server) — Webserver using this package to support Cache Digests via the `cache-digest` header or cookie.
- [cache-digest-immutable](https://www.npmjs.com/package/cache-digest-immutable) — Service worker to calculate cache digests client side. Does HTTP caching with special support for `cache-control: immutable`.
- [H2O](https://h2o.examp1e.net) — Webserver with C-language Cache Digest implementation.
- [cache-digest.js](https://github.com/h2o/cache-digest.js) — Original JavaScript implementation. Borrowing its BitCoder.
